import React, { useState, useEffect } from 'react'
import ProductTable from '../../component/ProductTable'
import ProductTitle from '../../component/ProductTitle'
import ProductCreate from '../../component/ProductCreate'
import { getAllProduct, deleteUser } from '../../api/api'

export default function Product(props) {
  const [user, setUser] = useState([])

  const fetchUser = async () => {
    let result = await getAllProduct()
    console.log(result)
    setUser(result.data)
  }

  useEffect(() => {
      fetchUser()
  }, [])

  const nextCreateProduct = () => {
    props.history.push('/createProduct')
  }

  const removeUser = async (id) => {
   let check = window.confirm("คุณต้องการลบหรือไม่ ?")
    if(check === true) {
      let result = await deleteUser(id)
      console.log(result)
      if (result.status === "success") {
        fetchUser()
      }
    }
  }

  return (
    <div>
      <ProductCreate nextCreateProduct={nextCreateProduct}/>
      <ProductTitle/>
      <ProductTable user={user} delete={removeUser}/>
    </div>
  )
}
