import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { getUserById } from "../../api/api";
var id = localStorage.getItem('Id')
export default function Profile(props) {
  const [userProfile, setUserProfile] = useState([]);

// eslint-disable-next-line react-hooks/exhaustive-deps
const fetchUser = async () => {
    await getUserById(localStorage.getItem('Id')).then((res) => {
      if (res.status === "success") {
        setUserProfile(res.data);
       
      }
    }); 
  };
  useEffect(() => {
    fetchUser();
  }, [fetchUser]);

  return (
    <div   style={{ textAlign: "center" }}>
        <h1>My Profile</h1>
            <hr/>
           
        <img
          src={process.env.PUBLIC_URL + "assets/images/logo1.png"}
          style={{ width: "200px", height: "200px"}}
        ></img>
     
        <div className="container">
          {
            <div>
              <div className="row">
                <div className="col-12 d-flex " >
                  
                </div>
              </div>
              <div className="row">
                <div className="col-12" >
                <p><strong><h2>Name: {userProfile.name}{" "} </h2></strong></p>
                  <p><strong>Age: </strong> {userProfile.age}{" "}</p>
                  <p><strong>Salary: </strong> {userProfile.salary}</p>
                </div>
              </div>
              <div className="btn-edit">
                    <Link to={`/edit/${userProfile._id}`} className="btn btn-outline-light btn-success">
                      Edit Profile
                    </Link>
                   </div>
            
            </div>
          }
        </div>
      
    </div>
  );
}