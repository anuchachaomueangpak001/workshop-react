import React, { useState } from 'react'
import { Link } from "react-router-dom";
import Axios from "axios";


// export default function Hometable(props) {
//   const [keyword, setKeyword] = useState("");

//     return (
//         <div>
          
//             <div>
//             <div class="row">
//                 <div class="col" style={{textAlign:'left'}}>
//                 <form className="form-inline my-2 my-lg-0 mb-3 ">
//                 <input onChange={(event) =>setKeyword(event.target.value)} className="form-control mr-sm-2 " type="search" placeholder="Search" aria-label="Search"/>
//                </form>
//                 </div>
//                 <div class="col" style={{textAlign:'right'}}>
//                 <Link type="button" to={`/myproduct/${props.user._id}`} className="btn btn-outline-success my-2 my-sm-0">
//                       My Product
//                 </Link>
//                 </div>
//             </div>
//             </div><br/>
//             <table className="table table-hover">
//         <thead>
//           <tr>
//             <th scope="col">No.</th>
//             <th scope="col">Title</th>
//             <th scope="col">Detail</th>
//             <th scope="col">Stock</th>
//             <th scope="col">Price</th>
//             {/* <th scope="col">Action</th> */}


//           </tr>
//         </thead>
//         <tbody>
//           {
//             props.user
//             .filter((item) => {
//                 return (
//                   item.title == keyword ||
//                   item.detail == keyword ||
//                   item.stock == keyword ||
//                   item.price == keyword
//                 );
//               })
//               .map((item, index) => (
//               <tr>
//               <th scope="row">{ index + 1 }</th>
//               <td>{item.title}</td>
//               <td>{item.detail}</td>
//               <td>{item.stock}</td>
//               <td>{item.price}</td>

//               <td>
//                 {/* <Link to={`/productDetail/${item._id}`}>
//                   <span style={{ color: "green" }}>Detail</span>
//                 </Link> */}
//                 {/* | */}
//                 {/* <span onClick={() => props.delete(item._id)} style={{ color: "red", cursor: 'pointer' }}>Delete</span> */}
//               </td>

//             {/* <td>
//                 <button type="submit" class="btn btn-success" >Detail</button>
                
//             </td> */}
//             </tr>
//             ))
//           }
//         </tbody>
//       </table>
      
//         </div>
    
//     )
// }


// import { Link } from "react-router-dom";
// import React, { useState } from 'react';
// import { Table } from 'reactstrap';


export default function Hometable(props) {
  const [keyword, setKeyword] = useState("");

  return (
    <div >
      <table className="table table-hover">
      <div class="col" style={{textAlign:'left'}}>
                 <form className="form-inline my-2 my-lg-0 mb-3 ">
                 <input onChange={(event) =>setKeyword(event.target.value)} className="form-control mr-sm-2 " type="search" placeholder="Search" aria-label="Search"/>
               </form>
                 </div>
                 
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Username</th>
            <th scope="col">Name</th>
            <th scope="col">Age</th>
            <th scope="col">Salary</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {
            
            props.user.map((item, index) => (
              <tr>
              <th scope="row">{ index + 1 }</th>
              <td>{item.username}</td>
              <td>{item.name}</td>
              <td>{item.age}</td>
              <td>{item.salary}</td>
              <td>
                <Link to={`/edit/${item._id}`} class="badge badge-success">
                  Edit
                </Link>
                |
                <Link onClick={() => props.delete(item._id)} class="badge badge-danger">
                Delete
                </Link>
              </td>
            </tr>
            ))
          }
        </tbody>
      </table>
    </div>
  );
}