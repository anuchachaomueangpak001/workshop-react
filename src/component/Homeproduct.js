import { Link } from "react-router-dom";
import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import HomeTitle from "./HomeTitle";
import Hometable from "../component/HomeTable";
import Home from "../pages/Home/Home";
import Product from "../pages/Home/Product";


const Example = (props) => {
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }

  return (
    <div className="mt-5" >
      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          > Product
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
         MY Product
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <Row>
            <Col sm="12">
              <div style={{ textAlign: "center" }}> 
              <Home/>
             </div>
            </Col>
          </Row>
        </TabPane>
        <TabPane tabId="2">
          <Row>
            <Col sm="12">
              <div style={{ textAlign: "center" }}> 
              <Product/>
             </div>
            </Col>
          </Row>
        
        </TabPane>
      </TabContent>
    </div>
    
  );
}

export default Example;