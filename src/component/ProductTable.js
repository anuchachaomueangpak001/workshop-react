import React, {useState} from 'react'
import { Link } from "react-router-dom";


export default function ProductTable(props) {
  const [keyword, setKeyword] = useState("");

    return (
        <div>
            <div>
            <div class="row">
                <div class="col" style={{textAlign:'left'}}>
                <form className="form-inline my-2 my-lg-0 mb-3 ">
                <input onChange={(event) =>setKeyword(event.target.value)} className="form-control mr-sm-2 " type="search" placeholder="Search" aria-label="Search"/>
               </form>
                </div>
                
            </div>
            </div><br/>
            <table class="table table-striped table-dark">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Title</th>
            <th scope="col">Detail</th>
            <th scope="col">Stock</th>
            <th scope="col">Price</th>
            {/* <th scope="col">Action</th> */}


          </tr>
        </thead>
        <tbody>
          {
            props.user
            .filter((item) => {
                return (
                  item.title == keyword ||
                  item.detail == keyword ||
                  item.stock == keyword ||
                  item.price == keyword
                );
              })
              .map((item, index) => (
              <tr>
              <th scope="row">{ index + 1 }</th>
              <td>{item.title}</td>
              <td>{item.detail}</td>
              <td>{item.stock}</td>
              <td>{item.price}</td>

              <td>
                {/* <Link to={`/productDetail/${item._id}`}>
                  <span style={{ color: "green" }}>Detail</span>
                </Link> */}
                {/* | */}
                {/* <span onClick={() => props.delete(item._id)} style={{ color: "red", cursor: 'pointer' }}>Delete</span> */}
              </td>

            {/* <td>
                <button type="submit" class="btn btn-success" >Detail</button>
                
            </td> */}
            </tr>
            ))
          }
        </tbody>
      </table>
        </div>
    
    )
}








// import React, { useState } from 'react'
// import { Link } from "react-router-dom";
// import Axios from "axios";


// export default function ProductTable(props) {
//   const [keyword, setKeyword] = useState("");
//   // useState =(keyword)=>{
//   //   console.log(keyword)
//   //   Axios.get('http://203.154.59.14:3000/api/v1/products').then(result=>{
//   //     console.log(JSON.sstringify( result.data.result))
//   //   })
//   // }
//   return (
    
//     <div>
//       <input style={{fonsize: 24,display: 'black', width: '100%', paddingLef: 8}}
//         placeholder="Enter your Keyword"
//         onChange={(event)=>{ console.log(event.target.value)} }/>
//       <table className="table table-striped table-dark">
//         <thead>
//           <tr>
//             <th scope="col">number</th>
//             <th scope="col">ID</th>
//             <th scope="col">Title</th>
//             <th scope="col">Detail</th>
//             <th scope="col">Stock</th>
//             <th scope="col">Price</th>
//             <th scope="col">Actions</th>
//           </tr>
//         </thead>
//         <tbody>
//           {
//             props.user.map((item, index) => (
//               <tr>
//               <th scope="row">{ index + 1 }</th>
//               <td>{item.user_id}</td>
//               <td>{item.title}</td>
//               <td>{item.detail}</td>
//               <td>{item.stock}</td>
//               <td>{item.price}</td>
//               <td>
//                 <Link to={`/edit/${item._id}`}>
//                   <span style={{ color: "green" }}>Edit</span>
//                 </Link>
//                 |
//                 <span onClick={() => props.delete(item._id)} style={{ color: "red", cursor: 'pointer' }}>Delete</span>
//               </td>
//             </tr>
//             ))
//           }
//         </tbody>
//       </table>
//     </div>
//   );
// }
