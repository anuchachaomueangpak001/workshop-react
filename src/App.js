import React from 'react';
import Header from './component/Header'
import Register from './pages/Register/Register'
import Home from './pages/Home/Home'
import Create from './pages/Create/Create'
import CreateProduct from './pages/Create/CreateProduct'
import Edit from './pages/Edit/Edit'
import Product from './pages/Home/Product'
import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom'
import PrivateRoute from './helper/PrivateRoute'
import  Homeform from './pages/Home/Homeform'
import Login from './pages/Login/Login'
import Headerlogin from './component/Headerlogin';
import Homeproduct from "./component/Homeproduct";
import Profile from "./component/Profile";


var routes = {
  homeform: '/homeform',
  login: '/login',
  register: '/register',
  home: '/home',
  create: '/create',
  edit: '/edit/:id',
  product: '/product',
  createProduct:'/createProduct',
  Homeproduct:'/Homeproduct',
  Profile:'/Profile'
 
}
function App() {
  return (
    <div>
    <Header/>
      <div className="container">
          <Switch>
              <PrivateRoute exact path={routes.home} component={Home}></PrivateRoute>
              <PrivateRoute exact path={routes.create} component={Create}></PrivateRoute>
              <PrivateRoute exact path={routes.edit} component={Edit}></PrivateRoute>
              <PrivateRoute exact path={routes.product} component={Product}></PrivateRoute>
              <PrivateRoute exact path={routes.createProduct} component={CreateProduct}></PrivateRoute>
              <PrivateRoute exact path={routes.homeform} component={Homeform}></PrivateRoute>
              <PrivateRoute exact path={routes.Homeproduct} component={Homeproduct}></PrivateRoute>
              <PrivateRoute exact path={routes.Profile} component={Profile}></PrivateRoute>

             

          </Switch>
      </div>
      <div className="container login">
          <Switch> 
            <Redirect exact from="/" to={routes.homeform}></Redirect>
            <Route exact path={routes.login} component={Login}></Route>
            <Route exact path={routes.register} component={Register}></Route>
          </Switch>
      </div>
    </div>
  );
}
export default App;